package com.cihangir.resources;

import com.cihangir.model.Book;
import com.cihangir.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/book")
public class BookResource {

	@Autowired
	private BookService bookService;
	
	@RequestMapping("/bookList")
	public List<Book> getBookList(Model model) {

		// get books from the service
		List<Book> bookList=bookService.findAll();
		
		return bookList;

	}
	
	@RequestMapping (value="/add", method=RequestMethod.POST)
	public Book addBookPost(@RequestBody Book book) {
		System.out.println(book.getAuthor());
		return bookService.save(book);
	}
	
	@RequestMapping("/{id}")
	public Book getBook(@PathVariable("id") Long id){
		Book book = bookService.findOne(id);
		return book;
	}
	
	@RequestMapping(value="/remove", method=RequestMethod.POST)
	public ResponseEntity remove(@RequestBody String id) throws IOException {
		bookService.deleteBook(Long.parseLong(id));
		return new ResponseEntity("Remove Success!", HttpStatus.OK);
	}
	
	
}
